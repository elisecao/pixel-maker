const heightInput = document.querySelector("#height");
const widthInput = document.querySelector("#width");
const colorInput = document.querySelector("#color-picker");

const canvas = document.querySelector("#canvas");
const submit = document.querySelector("button[type=submit]");
const reset = document.querySelector("#reset-canvas");

const zoomInBtn = document.querySelector("#zoom-in");
const zoomResetBtn = document.querySelector("#zoom-reset");
const zoomOutBtn = document.querySelector("#zoom-out");

// ZOOM RELATED FUNCTIONS
function setBoxSize(box, height, width) {
  box.style.height = height + "rem";
  box.style.width = width + "rem";
}

function getBoxSize(box) {
  if (box.style.height === "") return [2, 2];

  const height = +box.style.height.slice(0, -3);
  const width = +box.style.width.slice(0, -3);
  return [height, width];
}

function performOnBoxes(func) {
  const boxes = document.querySelectorAll(".box");
  boxes.forEach(box => func(box));
}
function modifyBoxSize(box, value) {
  const [height, width] = getBoxSize(box);
  setBoxSize(box, height + value, width + value);
}

zoomInBtn.addEventListener("click", () => {
  performOnBoxes(box => modifyBoxSize(box, 1));
});

zoomResetBtn.addEventListener("click", () => {
  performOnBoxes(box => setBoxSize(box, 2, 2));
});

zoomOutBtn.addEventListener("click", () => {
  performOnBoxes(box => modifyBoxSize(box, -1));
});

// DRAW RELATED FUNCTIONS

// Generate a Box which is <div class="box">
function makeBox() {
  const element = document.createElement("div");
  element.classList.add("box");
  return element;
}

// Generate many Boxes
function createBox(row, col) {
  const nBox = row * col;
  return [...Array(nBox).keys()].map(_ => makeBox());
}

// Fill box with the current color pick
function fillBox(event) {
  event.preventDefault();
  if (event.which === 1) {
    // fill
    this.style.border = 0;
    this.style.backgroundColor = colorInput.value;
  } else if (event.which === 3) {
    // remove color
    this.style.border = "1px solid black";
    this.style.backgroundColor = "transparent";
  }
}

// Experimental for touch screen device
function touchBox(event) {
  this.style.border = 0;
  this.style.backgroundColor = colorInput.value;
}

// Size Button Clicked
// 1. Generate Boxes
// 2. Add Click Event Listeners
submit.addEventListener("click", function(event) {
  event.preventDefault();
  resetCanvas(canvas);
  prepare();

  const row = heightInput.value;
  const col = widthInput.value;

  const boxes = createBox(row, col);
  boxes.map(box => {
    box.addEventListener("click", fillBox);
    box.addEventListener("contextmenu", fillBox);
    box.addEventListener("mousemove", fillBox);
    canvas.appendChild(box);
  });

  setRow(canvas, row);
  setCol(canvas, col);
});

// Hide Grid Size menu
// Open canvas
function prepare() {
  const sections = document.querySelectorAll("section");
  sections.forEach(section => {
    if (section.className === "canvas" || section.className === "color")
      section.style.display = "flex";
    else if (section.className === "size") section.style.display = "none";
    else section.style.display = "block";
  });
}

// Show Grid Size Menu and Hide everything else
function unprepare() {
  const sections = document.querySelectorAll("section");
  sections.forEach(section => {
    // Hide except size control
    section.style.display = "";
  });
}

// Set Row in Canvas
function setRow(canvas, n) {
  canvas.style.gridTemplateRows = `repeat(${n}, 1fr)`;
}

// Set Col in Canvas
function setCol(canvas, n) {
  canvas.style.gridTemplateColumns = `repeat(${n}, 1fr)`;
}

// Delete all div in canvas
function resetCanvas(canvas) {
  while (canvas.firstChild) {
    canvas.removeChild(canvas.firstChild);
  }
}

// Delete Everything and Show Size Menu
reset.addEventListener("click", event => {
  event.preventDefault();
  resetCanvas(canvas);
  unprepare();
});

// Register Color Shortcut
const colorSection = document.querySelector(".color");
document.addEventListener("keydown", event => {
  // Pressed C
  if (event.keyCode === 67 && colorSection.style.display === "flex") {
    console.log("click color");
    colorInput.click();
  }
});

// disable right click
document.addEventListener("contextmenu", event => event.preventDefault());
