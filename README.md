# Get Started

## Install

```bash
yarn
```

## Serve

```bash
yarn start
```

## Build

```bash
yarn build
```
